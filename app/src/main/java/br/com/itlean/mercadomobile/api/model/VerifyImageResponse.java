package br.com.itlean.mercadomobile.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by marcoscardoso on 11/11/17.
 */

public class VerifyImageResponse {

    @Expose
    public Classified classified;
    @SerializedName("imageUrl")
    @Expose
    public String imageUrl;
    @SerializedName("result")
    @Expose
    public String result;


    public class Classified {

        @SerializedName("no_cancer")
        @Expose
        public String noCancer;

        @SerializedName("have_cancer")
        @Expose
        public String haveCancer;
    }
}

package br.com.itlean.mercadomobile.api;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by marcoscardoso on 09/03/17.
 */

public class ApiAuthInterceptor implements Interceptor {
    private static final String LOGTAG = ApiAuthInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();


        Request.Builder builder = originalRequest.newBuilder()
                .method(originalRequest.method(), originalRequest.body());

        Log.d(LOGTAG, "URL : " + originalRequest.url());

        return chain.proceed(builder.build());
    }
}

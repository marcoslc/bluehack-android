package br.com.itlean.mercadomobile.api;


import br.com.itlean.mercadomobile.api.model.UploadImageResponse;
import br.com.itlean.mercadomobile.api.model.VerifyImageResponse;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by marcoscardoso on 09/03/17.
 */

public interface APIService {

    @GET("a39769cb-ca0f-426d-8f52-d2ce220c49fb")
    Call<VerifyImageResponse> sendImage(@Query("imageUrl") String imageUrl);

    @Multipart
    @Headers("authorization:Client-ID d72afd50b2d0e52")
    @POST()
    Call<UploadImageResponse> uploadImage(@Url String url, @Part MultipartBody.Part file);
}
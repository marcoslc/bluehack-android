package br.com.itlean.mercadomobile.api;


import br.com.itlean.mercadomobile.BuildConfig;
import br.com.itlean.mercadomobile.api.APIResponse;
import br.com.itlean.mercadomobile.api.APIService;
import br.com.itlean.mercadomobile.api.ApiAuthInterceptor;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by marcoscardoso on 09/03/17.
 */

public class APIFactory {

    public static final String BASE_URL = "http://177.220.10.134:20256/powerai-vision/api/dlapis/";


    public static APIService getService() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new ApiAuthInterceptor())
                .build();

        HttpUrl url = HttpUrl.parse(BASE_URL);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit.create(APIService.class);
    }
}

package br.com.itlean.mercadomobile.api;

/**
 * Created by marcoscardoso on 09/03/17.
 */

public class APIResponse {

    public int id;
    public String cif,tituloNotas;
    public String status;
    public String errorMessage;
    public Object result;
    public int errorCode;
}

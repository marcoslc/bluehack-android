package br.com.itlean.mercadomobile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

/**
 * Created by marcoscardoso on 11/11/17.
 */

public class CameraActivity extends AppCompatActivity {
    public static final String LOG_TAG = "NiceCameraExample";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    public static final int RESULT_LOAD_IMAGE = 2;


    private Camera camera;
    private int cameraID;
    private CameraPreview camPreview;

    AppCompatImageView imageview;

    ProgressDialog waitDialog;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        imageview = (AppCompatImageView) findViewById(R.id.imageLoaded);

        AppCompatButton fromGalleryBt = findViewById(R.id.from_gallery);
        fromGalleryBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        getSupportActionBar().hide();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File file = new File(picturePath);
            camPreview.uploadImage(file);

            waitDialog = new ProgressDialog(CameraActivity.this);
            waitDialog.setMessage("Analizando Imagem");
            waitDialog.show();
        }
    }

    private void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);

        } else {
            startCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startCamera();
                } else {
                    checkPermission();
                }
                return;
            }
        }
    }

    private void startCamera() {
        if (setCameraInstance() == true) {
            this.camPreview = new CameraPreview(this, this.camera, this.cameraID);
            Camera.Parameters params = camera.getParameters();
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            //some more settings
            camera.setParameters(params);

            camPreview.setImageFileLoaded(new CameraPreview.ImageFileLoaded() {
                @Override
                public void imageFile(File file, String noCancer, String haveCancer) {

                    if (waitDialog != null && waitDialog.isShowing())
                        waitDialog.dismiss();

                    if (file == null)
                        return;

                    String mensagem = getResources().getString(R.string.result_exam_message_dialog);
                    Float percent = 0f;

                    if (noCancer != null) {
                        percent = 1 - Float.parseFloat(noCancer);
                    } else {
                        percent = Float.parseFloat(haveCancer);
                    }

                    Toast.makeText(CameraActivity.this, mensagem, Toast.LENGTH_LONG).show();

                    LayoutInflater inflater = (LayoutInflater) CameraActivity.this.getSystemService
                            (Context.LAYOUT_INFLATER_SERVICE);

                    View view = inflater.inflate(R.layout.result_cancer_layout, null);

                    AppCompatImageView imageView = view.findViewById(R.id.image);
                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    imageView.setImageBitmap(myBitmap);

                    TextView textView = view.findViewById(R.id.percent);
                    int perInt = (int) (percent * 100);
                    textView.setText(perInt + "%");

                    TextView message = view.findViewById(R.id.message);
                    message.setText(mensagem);

                    (new AlertDialog.Builder(CameraActivity.this)).setView(view).show();
                }
            });
        } else {
            this.finish();
        }

        RelativeLayout preview = findViewById(R.id.preview_layout);
        preview.addView(this.camPreview);

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    camera.autoFocus(null);
                } catch (Exception e) {

                }
            }
        });

        RelativeLayout.LayoutParams previewLayout = (RelativeLayout.LayoutParams) camPreview.getLayoutParams();
        previewLayout.width = LayoutParams.MATCH_PARENT;
        previewLayout.height = LayoutParams.MATCH_PARENT;
        this.camPreview.setLayoutParams(previewLayout);

        // on the main activity there's also a "capture" button, we set its behavior
        // when it gets clicked here
        Button captureButton = (Button) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        waitDialog = new ProgressDialog(CameraActivity.this);
                        waitDialog.setMessage("Analizando Imagem");
                        waitDialog.show();
                        camera.takePicture(null, null, camPreview); // request a picture
                    }
                }
        );

        // at last, a call to set the right layout of the elements (like the button)
        // depending on the screen orientation (if it's changeable).
        fixElementsPosition(getResources().getConfiguration().orientation);
    }

    // following: a set of overwritten methods in order to start and stop the
    // camera when the app gets closed or something

    @Override
    protected void onResume() {
        super.onResume();

        try {
            checkPermission();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCameraInstance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseCameraInstance();
    }

    /**
     * This method is added in order to detect changes in orientation.
     * If we want we can react on this and change the position of
     * some GUI elements (see {@link #fixElementsPosition(int)}
     * method).
     */
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        fixElementsPosition(newConfig.orientation);
    }

    /**
     * [IMPORTANT!] The most important method of this Activity: it asks for an instance
     * of the hardware camera(s) and save it to the private field {@link #camera}.
     *
     * @return TRUE if camera is set, FALSE if something bad happens
     */
    private boolean setCameraInstance() {

        if (this.camera != null) {
            // do the job only if the camera is not already set
            Log.i(LOG_TAG, "setCameraInstance(): camera is already set, nothing to do");
            return true;
        }

        // warning here! starting from API 9, we can retrieve one from the multiple
        // hardware cameras (ex. front/back)
        if (Build.VERSION.SDK_INT >= 9) {

            if (this.cameraID < 0) {
                // at this point, it's the first time we request for a camera
                Camera.CameraInfo camInfo = new Camera.CameraInfo();
                for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                    Camera.getCameraInfo(i, camInfo);

                    if (camInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                        // in this example we'll request specifically the back camera
                        try {
                            this.camera = Camera.open(i);
                            this.cameraID = i; // assign to cameraID this camera's ID (O RLY?)
                            return true;
                        } catch (RuntimeException e) {
                            // something bad happened! this camera could be locked by other apps
                            Log.e(LOG_TAG, "setCameraInstance(): trying to open camera #" + i + " but it's locked", e);
                        }
                    }
                }
            } else {
                // at this point, a previous camera was set, we try to re-instantiate it
                try {
                    this.camera = Camera.open(this.cameraID);
                } catch (RuntimeException e) {
                    Log.e(LOG_TAG, "setCameraInstance(): trying to re-open camera #" + this.cameraID + " but it's locked", e);
                }
            }
        }

        // we could reach this point in two cases:
        // - the API is lower than 9
        // - previous code block failed
        // hence, we try the classic method, that doesn't ask for a particular camera
        if (this.camera == null) {
            try {
                this.camera = Camera.open();
                this.cameraID = 0;
            } catch (RuntimeException e) {
                // this is REALLY bad, the camera is definitely locked by the system.
                Log.e(LOG_TAG,
                        "setCameraInstance(): trying to open default camera but it's locked. "
                                + "The camera is not available for this app at the moment.", e
                );
                return false;
            }
        }

        // here, the open() went good and the camera is available
        Log.i(LOG_TAG, "setCameraInstance(): successfully set camera #" + this.cameraID);
        return true;
    }

    /**
     * [IMPORTANT!] Another very important method: it releases all the resources and the locks
     * we created while using the camera. It MUST be called everytime the app exits, crashes,
     * is paused or whatever. The order of the called methods are the following: <br />
     * <p>
     * 1) stop any preview coming to the GUI, if running <br />
     * 2) call {@link Camera#release()} <br />
     * 3) set our camera object to null and invalidate its ID
     */
    private void releaseCameraInstance() {
        if (this.camera != null) {
            try {
                this.camera.stopPreview();
            } catch (Exception e) {
                Log.i(LOG_TAG, "releaseCameraInstance(): tried to stop a non-existent preview, this is not an error");
            }

            this.camera.setPreviewCallback(null);
            this.camera.release();
            this.camera = null;
            this.cameraID = -1;
            Log.i(LOG_TAG, "releaseCameraInstance(): camera has been released.");
        }
    }

    /**
     * Everytime the screen changes its orientation, the layout of the
     * snap button changes to a more convenient position.
     *
     * @param orientation
     */
    private void fixElementsPosition(int orientation) {
        Button captureButton = (Button) findViewById(R.id.button_capture);
        FrameLayout.LayoutParams layout = (FrameLayout.LayoutParams) captureButton.getLayoutParams();

        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                layout.gravity = (Gravity.RIGHT | Gravity.CENTER);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                layout.gravity = (Gravity.BOTTOM | Gravity.CENTER);
                break;
        }
    }

    public Camera getCamera() {
        return this.camera;
    }

    public int getCameraID() {
        return this.cameraID;
    }
}
